# jacoco2cobertura

Docker image to allow java projects that use jacoco to use the new codecoverage feature of gitlab.

The image includes 2 scripts.
* cover2cover.py (forked from https://github.com/rix0rrr/cover2cover ; includes open PRs in that repo)
  * Converts jacoco xml reports to cobertura xml reports
* sourc2filename.py
  * reads the `<source></source>` tag and prepends the path to the filename attribute of each class.  
    This is necessary, because gitlab currently ignores that tag and expects the filename attribute to include the full path from the project root.  
    

# Usage:

```yaml
stages:
  - build
  - test
  - visualize
  - deploy

test-jdk11:
  stage: test
  image: maven:3.6.3-jdk-11
  script:
    - 'mvn $MAVEN_CLI_OPTS clean org.jacoco:jacoco-maven-plugin:prepare-agent test jacoco:report'
  artifacts:
    paths:
      - target/site/jacoco/jacoco.xml

coverage-jdk11:
  stage: visualize
  image: haynes/jacoco2cobertura:1.0.3
  script:
    - 'python /opt/cover2cover.py target/site/jacoco/jacoco.xml src/main/java > target/site/coverage.xml'
    - 'python /opt/source2filename.py target/site/coverage.xml'
  needs: ["test-jdk11"]
  dependencies:
    - test-jdk11
  artifacts:
    reports:
      cobertura: target/site/coverage.xml
```

# Multi modules:

Use report-aggregate for jacoco-maven-plugin. May by add module for jacoco. If you use 
like this structure:

* dao
  * dao-api
  * dao-impl
* core
  * core-api
  * core-impl
* web
* main
  * production
  * develop

Or don`t have one module evidently depend of all. Like web production depend of
web core-impl dao-impl, core-impl depend of dao-api and core-api. 
```xml
   ...
    <groupId>...</groupId>
    <artifactId>jacoco</artifactId>
    <dependencies>
        <dependency>
            <groupId>...</groupId>
            <artifactId>dao-api</artifactId>
            <version>...</version>
        </dependency>

        <dependency>
            <groupId>...</groupId>
            <artifactId>dao-impl</artifactId>
            <version>...</version>
        </dependency>

        <dependency>
            <groupId>...</groupId>
            <artifactId>core-api</artifactId>
            <version>...</version>
        </dependency>

        <dependency>
            <groupId>...</groupId>
            <artifactId>core-impl</artifactId>
            <version>...</version>
        </dependency>

        <dependency>
            <groupId>...</groupId>
            <artifactId>web</artifactId>
            <version>...</version>
        </dependency>

        <dependency>
            <groupId>...</groupId>
            <artifactId>developer</artifactId>
            <version>...</version>
        </dependency>

        <dependency>
            <groupId>...</groupId>
            <artifactId>production</artifactId>
            <version>...</version>
        </dependency>

    </dependencies>

```
```yaml
stages:
  - build
  - test
  - visualize
  - deploy

test-jdk11:
  stage: test
  image: maven:3.6.3-jdk-11
  script:
    - 'mvn $MAVEN_CLI_OPTS clean 
                           org.jacoco:jacoco-maven-plugin:prepare-agent
                           test
                           org.jacoco:jacoco-maven-plugin:report-aggregate'
  after_script:
    - cat jacoco/target/site/jacoco-aggregate/index.html | grep -o '<tfoot>.*</tfoot>'
  artifacts:
    paths:
      - jacoco/target/site/jacoco-aggregate/jacoco.xml

coverage-jdk11:
  stage: visualize
  image: haynes/jacoco2cobertura:1.0.5
  script:
    # all module add to args
    - 'python /opt/cover2cover.py jacoco/target/site/jacoco-aggregate/jacoco.xml 
              dao/dao-api/src/main/java
              dao/dao-impl/src/main/java
              core/core-api/src/main/java
              core/core-impl/src/main/java
              web/src/main/java
              main/develop/src/main/java
              main/production/src/main/java
              > jacoco/target/site/coverage.xml'
    - 'python /opt/source2filename.py jacoco/target/site/coverage.xml'
  needs: ["test-jdk11"]
  dependencies:
    - test-jdk11
  artifacts:
    reports:
      cobertura: jacoco/target/site/coverage.xml
```
